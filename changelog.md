# 0.2.1

## Changes

- Added a setting for adding a default combat music

## Bug Fixes

- When issued a command to play a music while it was fading out resulted in the music not playing

# 0.2.0

Update for Foundry v10

## Changes

- Allow dragging specific tracks onto a music configuration sheet, immediately setting the track in addition to the playlist

## API

- Removed `game.vgmusic`
- Added `vgmusic` as a global variable
- Removed reliance on sockets, and instead synchronizes using Foundry-native functions

# 0.1.2

## Bug Fixes

- Fixed playback of music of 10 minutes or longer in duration

# 0.1.1

## Changes

- Added keyboard shortcuts for supressing area and combat music

## API

- Added the `game.VGMusic.PlaylistContext` class, used for determining the current playlist
- `game.VGMusic.MusicController.getAllCurrentPlaylists` now returns an array of `PlaylistContext`
