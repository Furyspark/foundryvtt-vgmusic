# Video Game Music

Adds music playing powers to FoundryVTT like it's a gosh-darn video game.

Once music managed by this module is playing again after being interupted
(by combat, for example), it will resume at its last position.

## Installation

From the main menu in Foundry (before a world is open), click on
'Add-on Modules', then on 'Install Modules' below. Then paste the
following within the text field labelled Manifest URL within
the new window:

```
https://gitlab.com/Furyspark/foundryvtt-vgmusic/-/raw/master/src/module.json
```

## Usage

### Adding battle music to actors

For actors, their music is located on their titlebars.

![Actor Music Entry](doc/img/actor-music.jpg)

Clicking this should open the following window:

![Actor Music Selector](doc/img/actor-music-config.jpg)

Drag and drop a playlist from the sidebar to the Combat field, and it should now
should something like the following:

![Actor Music Selector with Playlist](doc/img/actor-music-config_filled.jpg)

Optionally, select a specific track for the playlist to start.

### Adding music to scenes

For scenes, their music is located within the following section:

![Scene Music Entry](doc/img/scene-music.jpg)

Clicking this button should open the following window:

![Scene Music Selector](doc/img/scene-music-config.jpg)

To add a playlist, drag one from the sidebar onto the appropriate field.

'Zone' music will play while this scene is active. 'Battle' music will play
during combat within this scene, and as long as there's no actor with their own
battle music participating in combat (though this could partially be changed
within the module configuration).

### Temporarily supressing music

Area music and combat music can be supressed individually by using
the following buttons on the sidebar panel:

![Sidebar Panel with Supressing Buttons](doc/img/sidebar-controls.jpg)
