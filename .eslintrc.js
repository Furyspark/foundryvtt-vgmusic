module.exports = {
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
  },

  env: {
    jquery: true,
    browser: true,
    node: true,
  },

  extends: ["eslint:recommended", "@typhonjs-fvtt/eslint-config-foundry.js/0.8.0", "plugin:prettier/recommended"],

  plugins: [],

  rules: {
    // Specify any specific ESLint rules.
  },

  overrides: [
    {
      files: ["./*.js"],
      env: {
        node: true,
      },
    },
  ],

  globals: {
    libWrapper: "readonly",
    vgmusic: true,
    Promise: "readonly",
    $: "readonly",
    CONFIG: "readonly",
  },
};
